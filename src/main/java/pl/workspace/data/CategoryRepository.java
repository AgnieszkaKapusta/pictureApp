package pl.workspace.data;

import org.springframework.stereotype.Component;
import pl.workspace.model.Category;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Aga on 2017-03-13.
 */

@Component
public class CategoryRepository {

    private static final List<Category> ALL_CATEGORIES = Arrays.asList(
            new Category(1, "funny"),
            new Category(2, "sport"),
            new Category (3, "IT")
    );

    public List<Category> getAllCategories(){
        return ALL_CATEGORIES;
    }

    public Category findById(int id){
        for(Category category : ALL_CATEGORIES){
            if(category.getId()==id)
                return category;
        }
        return null;
    }
}
