package pl.workspace.data;

import org.springframework.stereotype.Component;
import pl.workspace.model.Gif;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Aga on 2017-03-10.
 */

@Component
public class GifRepository {
    private static final List<Gif> ALL_GIFS = Arrays.asList(
        new Gif("android-explosion","aga", false, 1),
        new Gif("ben-and-mike","mike", false, 1),
        new Gif("book-dominos","ala", true, 2),
        new Gif("compiler-bot","jurek", true,2 ),
        new Gif("cowboy-coder","coder", false,3),
        new Gif("infinite-andrew","andrew", false,3)
    );

    public Gif findByName(String name){
        for (Gif gif : ALL_GIFS){
            if ( gif.getName().equals(name))
                return gif;
        }
        return null;
    }

    public List<Gif> getAllGifs(){
        return ALL_GIFS;
    }

    public List<Gif> getFavorites(){
        List<Gif> gifs = new ArrayList<Gif>();
        for (Gif gif : ALL_GIFS){
            if (gif.isFavorite())
                gifs.add(gif);
        }
        return gifs;
    }
}
