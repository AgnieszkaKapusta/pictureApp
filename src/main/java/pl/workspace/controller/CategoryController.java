package pl.workspace.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.workspace.data.CategoryRepository;
import pl.workspace.model.Category;

import java.util.List;

/**
 * Created by Aga on 2017-03-13.
 */

@Controller
public class CategoryController {

    @Autowired
    private CategoryRepository categoryRepository;

    @RequestMapping("/categories")
    public String listCategories(ModelMap modelMap){
        List<Category> categories =  categoryRepository.getAllCategories();
        modelMap.put("categories", categories);
        return "categories";
    }
}
